The tube calibration is the Tube1TransferFunction.pdf file was performed by two people.
One person sent commands to the STM board via a putty session over a serial connection
and recorded values while the other held the tube sideways and moved the ping pong ball
with a meter stick inserted into the top end of the tube. ADC measurements were taken
every 5cm according to the marks on the side of the tube with the ball centered on the
measurement mark. Several ADC measurements were taken, and the average measurement was
kept as the data point for the graph. It should be noted that one value that was
recorded didn't follow the same trend as the other values, so data around that point
may be inaccurate, but is relatively precise considering the small deviation in that
particular point.