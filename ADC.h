#ifndef __ADC_H__
#define __ADC_H__
#include "registers.h"
#include <stdint.h>

void ADCInit(void);
void EnableADCInt(void);
void DisableADCInt(void);
void StartADC(void);
uint16_t GetADC(void);
void ADC1_2_IRQHandler(void);

#endif
