#include "control.h"

//To be implemented later, presumably; this is a stub
void PControl(uint16_t adcval) {\
	uint16_t current, error, output;
	//Convert to distance (lookup table)
	current = adcval;//Temporary fixture
	//Find error
	error = SETPOINT-current;
	//Turn into percentage of valve opening
	output = error * CONTROL_GAIN * CONTROL_RANGE + CONTROL_MID;
	PWMAngleSet(output);
}
