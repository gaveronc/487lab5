#ifndef __CONTROL_H__
#define __CONTROL_H__
#include <stdint.h>
#include "commands.h"

static uint8_t ADC_FLAG = 0;//Conversion complete
static uint16_t ADC_VAL = 0;//Communication with main
static uint16_t SETPOINT = 0;//Valve set point, currently not used
static uint16_t CONTROL_GAIN = 1;//Control loop proportional gain
static uint16_t CONTROL_RANGE = 20;//Range of effective valve percentage
static uint16_t CONTROL_MID = 55;//Mid-point of valve percentage

#endif
