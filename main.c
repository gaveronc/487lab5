/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Lab 3
*/

#include <stdint.h>
#include "registers.h"
#include "UART.h" //Enable UART functions
#include "commands.h"
#include "timer.h"
#include "ADC.h"

/*
* This program implements a command prompt on the lab board
* that can be accessed over a serial COM port using putty
* or another similar serial terminal program.
* This program maps character arrays to function calls by
* taking all of the input up to a certain point and comparing
* it to an array of strings to try finding a match. When a
* complete match is found, everything following the function
* call is treated as the function's argument. To do this in a
* scalable way, each function, whether it uses arguments or
* not, has to take a pointer to a character array.
* 
* More important than the parsing methods, however, is the
* newly-implemented valve control features. First, the valve
* controls must be turned on with VALVE ON. Then the angle of
* the valve can be set with SET ANGLE <percentage>.
* 
*/

//Initialize clocks and GPIO pins
void SysInit(void) {
	//Need to setup the the LED's on the board
	RCC_APB2ENR |= 	1 << 3 |	// Enable Port B clock
									1 << 4 | 	// Enable Port C clock
									1 << 9;		// Enable ADC1 clock
	GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	GPIOB_CRL  = 0xB3333333;
	GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
}

int main(void)
{
	uint8_t commandByte;
	char command[COMMAND_LENGTH] = {0};
	unsigned charCount = 0;
	int k;//Count variables
	
	//Initialize clocks and GPIO pins
	SysInit();
	
	//Set up timers
	InitPWMTimer();
	
	//Initialize ADC
	ADCInit();
	
	Serial_Open();//Start serial communications
	
	SendLine(" \n\rSeaBiscuit > ");//Command prompt
	
	while(1) {
		//while(!ByteReady() && !ADC_FLAG);//Wait for command byte or ADC flag
		
		if(ADC_FLAG) {
			//Handle control system functions
			
			//Start another conversion
			StartADC();
		} else {
			//Command byte is ready
			commandByte = GetByte();
			if((commandByte == 0x8) || (commandByte == 0x7F)) {//Check for backspace
				if (charCount > 0) {
					SendByte(0x8);//Mirror the backspace
					SendByte(0x20);//Send a whitespace to clear a character
					SendByte(0x8);//Move back one spot
					
					//Remove the last byte in the command buffer and decrement count
					command[charCount] = 0;
					charCount -= 1;
				}
			} else {
				if (commandByte == 0xD) {//Check for enter key
					SendByte(0xA);//Send carriage return and line feed to the typewriter
					SendByte(0xD);
					
					if(charCount != 0) {
						SendCommand(command, 1);
						//Reset command string
						charCount = 0;
						for (k = 0; k < COMMAND_LENGTH; k += 1) {
							command[k] = 0;
						}
					}
					SendLine("SeaBiscuit > ");//Command prompt
				} else {
					if ((charCount < (COMMAND_LENGTH - 1)) && (commandByte >= 0x20)) {
						//Add the character to the command and increment counter, ignoring garbage characters
						//Also, capitalize any lower-case characters
						if ((commandByte >= 'a') && (commandByte <= 'z')) {
							commandByte -= 32;
						}
						command[charCount] = commandByte;
						charCount += 1;
						//Mirror the byte
						SendByte(commandByte);
					}
				}
			}
		}
	}
}
